var dots = [];
var x;
var start_x;
var y;
var start_y;

var points = [];
var xc;
var startc;
var yc;
var rad;
var step = 0;
var step_inc = 0.01;

var col_rect = [];
var col_point = [];

var s = 1.5;
var r = 1;

var square = false;

function setup(){
	var cnv = createCanvas(windowWidth, 200);

	start_x = width*0.45;
	x = start_x;
	start_y = height*0.2;
	y = start_y;

	rad = height*0.25;

	addDotSquare();
	addDotCircle();
	setInterval(addDotSquare, 10);
	setInterval(addDotCircle, 3);
}

function draw(){
	background(255);
	stroke(0);
	noStroke();

	if(square){
		rectMode(CENTER);
		for(var i = 0; i < dots.length; i++){
			fill(col_rect[i]);
			rect(dots[i].x, dots[i].y, r*2, r);
		}
	}else{
		for(var i = 0; i < points.length; i++){
			fill(col_point[i]);
			rect(points[i].x, points[i].y, r, r);
		}
	}

	rectMode(CORNER);
	fill(255);
	rect(width*0.45, 0, -200, 200);
	rect(width*0.55, 0, 200, 200);
}

function addDotSquare(){
	var d = createVector(x, y);


	if(x > width*0.55){
			y+=2;
			// x = start_x;
			// x = width*0.55;
			dots.push(d);
			s *= -1;
	}else if(x < width*0.45){
		y+=2;
		// x = start_x;
		// x = width*0.45;
		dots.push(d);
		s *= -1;
	}else if(y <= start_y+width*0.1){
		dots.push(d);
	}

	if(mouseY < 200){
		if(noise(millis()*0.01) > map(mouseX, 0, width, 0, 0.5)){
			col_rect.push(0)
		}else{
			col_rect.push(255);
		}
	}else{
		col_rect.push(0);
	}

	x+=s;
}

function addDotCircle(){
	step+=step_inc;

	if(step > TWO_PI || step < 0){
		rad-=2;
		step_inc *= -1;
		// step = 0;
	}

	xc = width*0.5+cos(step)*rad;
	yc = height*0.5+sin(step)*rad;

	var p = createVector(xc, yc);
	points.push(p);

	if(mouseY < 200){
		if(noise(millis()*0.01) > map(mouseX, 0, width, 0, 0.5)){
			col_point.push(0)
		}else{
			col_point.push(255);
		}
	}else{
		col_point.push(0);
	}
}

function mousePressed(){
	square = !square;
}
