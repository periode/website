var startTime;
var timer;

var refreshFrame = 500;

// var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
var characters = "-_|'";
var current_position;
var current_color;
var letters = [];
var hue_val = 0;

var fgc = 0;
var bgc = 255;

var step;
var size;

function setup(){
	var cnv = createCanvas(windowWidth, 200);
	background(255);

	size = 18;

	current_position = createVector(size, size);
	current_color = color(map(noise(millis()*0.005, 0), 0, 1, 100, 300));

	step = createVector(size*0.75, size);

	startTime = millis();
	timer = 5;
	noStroke();
	textSize(size*1.25);
}

function draw(){
	textFont("EdwardPro-ExtraLight");
	noStroke();
	if(checkMobile()){
		background(noise(millis()*0.001)*80+210);
		for(var i = 0; i < 100; i+=0.25){
			fill(255, i);
			rect(0, 100+i*4, width, 200-i*4);
		}
	}
	for(var i = 0; i < letters.length; i++){
		letters[i].display();
	}
	if(fgc === 0)
		current_color = color(pow(map(noise(millis()*0.009, 0), 0, 1, 10, 18), 2));
	else
		current_color = color(pow(map(noise(millis()*0.009, 0), 0, 1, 16, 2), 2));

	if(millis() - startTime > timer){
		if(letters.length > 1200)
			reset();

		var l = new Letter(getNewCharacter(), current_position, current_color);
		letters.push(l);
		startTime = millis();

		current_position.x += step.x;
		if(current_position.x > width){
			current_position.y += step.y;
			current_position.x = 0;
		}
	}

	if(!checkMobile()){
		if(mouseY < 200){
			for(var i =0 ; i < letters.length; i++){
				letters[i].col = color(red(letters[i].start_col) + map(mouseY, 0, 120, 150, 0) - map(mouseX, 0, width, 120, 0));
			}
		}
	}else{

		letters[i].col = color(red(letters[i].start_col) + map(40, 0, 120, 150, 0) - map(width*0.7, 0, width+cos(millis()*0.05)*width*0.25, 120, 0));
	}

}

function getNewCharacter(){
	return characters[Math.floor(Math.random()*characters.length)];
}

function mousePressed(){
	reset();
}

function checkMobile(){
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
    return true;
	}else{
		return false;
	}
}


var Letter = function(_char, _pos, _col){
	this.letter = _char;
	this.position = _pos.copy();
	this.start_col = _col;
	this.col = _col;

	this.display = function(){
		stroke(this.col);
		fill(this.col);
		text(this.letter, this.position.x, this.position.y);
	}
}

function reset(){
	// if(fgc === 0){
	// 	fgc = 255;
	// 	bgc = 0;
	// }else{
	// 	fgc = 0;
	// 	bgc = 255;
	// }
	// background(bgc);
	letters = [];
	current_position = createVector(size, size);
	// hue_val = fgc;
}
