var sine;
var a;
var b;

var inca;
var incb;

var posa = [];
var posb = [];

function setup(){
	var cnv = createCanvas(windowWidth, 200);
	frameRate(20);

	a = 0;
	b = width;

	posa[0] = createVector(0, height*0.5);
	posb[0] = createVector(width, height*0.5);

	inca = (Math.random()+1)*10;
	incb = (Math.random()+1)*10;
}

function draw(){
	background(255);
	noFill();
	strokeWeight(3);

	var pa = createVector(a, height*0.5+(sin(a)*height*0.3));
	a += inca;

	posa.push(pa);

	var pb = createVector(b, height*0.5+(cos(b)*height*0.3));
	b -= incb;

	posb.push(pb);

	stroke(200, 200, 250, 255-(mouseX/width)*235);
	if(posa != null && posa.length > 1){
		for(var i = 0; i < posa.length-1; i++){
			line(posa[i].x, posa[i].y, posa[i+1].x, posa[i+1].y);
		}
	}


	stroke(250, 200, 200, 20+(mouseX/width)*235);
	if(posb != null && posb.length > 1){
		for(var i = 0; i < posb.length-1; i++){
			line(posb[i].x, posb[i].y, posb[i+1].x, posb[i+1].y);
		}
	}

	if(a > width){
		posa = [];
		posa[0] = createVector(0, height*0.5);
		a = 0;
		inca = (Math.random()+1)*10;
	}

	if(b < 0){
		posb = [];
		posb[0] = createVector(width, height*0.5);
		b = width;
		incb = (Math.random()+1)*10;
	}
}
