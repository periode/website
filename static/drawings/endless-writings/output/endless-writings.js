var lines = [];
var point;
var step;
var scroll = 0;

var topics = [];
var possible_topics = ["me", "you", "him", "her", "us", "humanity", "others", "life", "love", "death", "friendship", "hope", "regret", "survival", "joy", "music", "spring", "winter"];
var possible_prefixes = ["the impossibility of ", "the certainty of ", "the delight of ", "the lack of ", "the representation of ", "the memory of ", "the absence of ", "what remains of ", "", "", "", "the whispers of ", "the reflections of ", "the presence of ", "the absence of "];
var line_thickness = 10;

var black_noise_coeff = 0.01;
var black_noise_thresh = 0.2;

var chapter = 1;

var canScroll = false;

var min_step = 8;
var max_step = 50;

function setup(){
  var cnv = createCanvas(windowWidth, windowHeight);

  point = createVector(width*0.025, height*0.25);
  step = random(min_step, max_step);
  topics.push(new Topic(point.y));
  point.y += height*0.1;
  setInterval(addLine, 100);
  rectMode(CENTER);
  // textSize(32);
  var c=document.getElementsByTagName("canvas")[0];
  var ctx=c.getContext("2d");
  ctx.font="30px Inconsolata";
}

function update(){
  for(var i = 0; i < lines.length; i++){
    if(lines[i].pos.y < abs(scroll))
    lines.splice(i, 1);
  }

  for(var i = 0; i < topics.length; i++){
    if(topics[i].h < abs(scroll))
    topics.splice(i, 1);
  }

  // step = noise(millis()*0.001)*60;

  black_noise_thresh = map(noise(millis()*0.00001), 0, 1, 0.1, 0.7);

  // line_thickness = noise(millis()*0.0000001)*20+1;

  // if(lines.length > 1 && lines[lines.length-1].pos.y > height){
  //   scroll -= height*0.1;
  // }
}

function draw(){
  noCursor();
  background(0);
  update();

  push();
  // rotate(HALF_PI);
  translate(0, -scroll);
  lines.forEach(function(e, i , a){
    e.display();
  });

  topics.forEach(function(e, i, a){
    e.display();
  });

  pop();
  rectMode(CORNER);
  fill(0);
  noStroke();
  rect(0, 0, width*0.04, height);
  rect(width*0.96, 0, width*0.05, height);
  rectMode(CENTER);

  if(canScroll)
    scroll += height*0.0004;
  // debug();
}

function debug(){
  fill(0, 200, 0);
  text(lines.length, 10, 10);
}

function addLine(){
  var p = createVector(point.x, point.y);
  var c;

  var r = Math.random();

  if(r > 0.002){
    if(r > 0.01){//continue
      if(point.x < width*0.05)
      c = 1;
      else{
        if(noise(millis()*black_noise_coeff) > black_noise_thresh)
        c = 1;
        else {
          c = 0;
        }
      }


      var l = new Line(p, step+2, c);
      point.x += step;
      lines.push(l);

      if(point.x > width*0.95){
        point.x = width*0.025;
        point.y += height*0.05;

        if(point.y > height*0.9){
          canScroll = true;
          // scroll += height*0.05;
        }
      }
    }else{//line break
      point.x = width*0.05;
      point.y += height*0.05;

      if(point.y > width*0.9){
        canScroll = true;
        // scroll += height*0.05;
      }
    }

  }else{
    point.y += height*0.2;
    point.x = width*0.025;
    chapter++;
    topics.push(new Topic(point.y));
    line_thickness = random(1, height*0.05-2);
    step = random(min_step, max_step);
    point.y += height*0.1;

    if(point.y > height*0.5){
      scroll += height*0.085;
    }
  }

}

function mouseReleased(){
  line_thickness = random(1, 100);
}

var Topic = function(_h){
  this.words = chapter+". on "+possible_prefixes[Math.floor(Math.random()*possible_prefixes.length)]+possible_topics[Math.floor(Math.random()*possible_topics.length)];
  this.h = _h;

  this.display = function(){
    noStroke();
    fill(255);
    text(this.words, width*0.05, this.h)
  }
}

var Line = function(_pos, _w, _c){
  this.pos = _pos;
  this.w = _w;
  this.h = line_thickness;
  this.col = 255*_c;

  if(Math.random() > 0.999)
  this.col = color(255, 0, 0);


  this.display = function(){
    stroke(this.col);
    fill(this.col);
    rect(this.pos.x, this.pos.y, this.w, this.h);
  }
}
