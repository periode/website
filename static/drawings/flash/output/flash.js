var x;
var y;

var pX;
var pY;
var pc1;
var pc2;

var qX;
var qY;
var qc1;
var qc2;

var sw = 0;
var a = 0;

var varC;
var t = 0;

function setup() {
  var cnv = createCanvas(displayWidth, displayHeight);
  background(255);
  x = -200;
  y = 0;
  a = 0;
}


function draw() {
  background(255, 5);
  noCursor();

  t+= 0.1;
  varC = (sin(t)+1)*128;

  pc1 = map(x, width, 0, -20, 20);
  pc2 = map(y, height, 0, -20, 20);

  pX = x;
  pY = y;
  qX = x + random(-100, 100);
  qY = y + random(-40, 40);
  x += random(0, 40);
  y += random(0, 40);
  strokeWeight(1);
  stroke(0, 0, 0, 100+a);
  line(pX, pY, x, y);
  stroke(240+pc1, 220+pc2, 0, 150+a);
  line(qX, qY, x+20, y+20);
  stroke(0, 190+pc2, 190+pc1, 50+(a/2));
 // line(qY, qX, x+20, y+20);

  if (x < 0 || x > width || y < 0 || y > height) {
    a += 10;
    sw += 0.2;
    fill(255, 255, 255, 90);
    rectMode(CENTER);
    push();
    rotate(-PI/4);
    pop();
    x = random(0, width);
    y = random(0, height);
  }
}
