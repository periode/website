var vertices = [];
var min_dist = 10;
var max_dist = 20;
var num = 100;

function setup(){
  var cnv = createCanvas(displayWidth, displayHeight, 'p2d');

  for(var x = 0; x < num; x++){
    for(var y = 0; y < num; y++){
       p = createVector(x-(num*0.5), y-(num*0.5));
       vertices.push(p);
    }

  }
}

function draw(){
  noCursor();
  background(255, 50);
  translate(width*0.5, height*0.5);
  scale(0.002+(cos(millis()*0.0001)+1));
  // rotate(0.000001*millis()*mouseY);
  scale(1);
  stroke(0, 200);

  beginShape();
  fill(255);
  noFill();

  for(var i = 0; i < vertices.length; i++){
    stroke(i % 255, 255);
   point(vertices[i].x+map(i, 0, vertices.length, -1000, 1000), vertices[i].y+map(i, 0, vertices.length, -1000, 1000)+((noise(millis()*0.0001*mouseY+i)-0.5)*0.3*mouseX));
  }
  endShape(CLOSE);
}
