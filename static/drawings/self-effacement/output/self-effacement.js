var lines = [];
var all_lines;

var xpos = [];
var ypos = [];
var max;

function setup() {
  var cnv = createCanvas(displayWidth, displayHeight);

  max = parseInt(random(5, 20));

  for(var i = 1; i < width; i+= width/max){
    xpos.push(i);
  }

  for(var i = 1; i < height; i+= height/max){
    ypos.push(i);
  }

  setTimeout(addLine, 500);
}

function draw() {
  background(0, 0, 0, 10);

  noCursor();

  for(var i = 0; i < lines.length; i++){
   lines[i].display();
  }
}

function addLine(){
  if(lines.length < max*2){
    lines.push(new Line());
    setTimeout(addLine, 500);
  }
}

var Line = function() {
  this.lerpVal = 0;
  this.lerpInc  = random(0.0035, 0.0075);

  this.pick = function() {
    var p;
    var r = Math.random();

    if (r < 0.25) {//left
      p = createVector(0, ypos[Math.floor(Math.random()*ypos.length)]);
      var i = Math.floor(Math.random()*ypos.length);
      this.start = createVector(0, ypos[i]);
      this.end = createVector(width, ypos[i]);
    } else if (r < 0.5) {//right
      p = createVector(width, ypos[Math.floor(Math.random()*ypos.length)]);
      var i = Math.floor(Math.random()*ypos.length);
      this.start = createVector(width, ypos[i]);
      this.end = createVector(0, ypos[i]);
    } else if (r < 0.75) {//top
      p = createVector(xpos[Math.floor(Math.random()*xpos.length)], 0);
      var i = Math.floor(Math.random()*xpos.length);
      this.start = createVector(xpos[i], 0);
      this.end = createVector(xpos[i], height);
    } else {
      p = createVector(xpos[Math.floor(Math.random()*xpos.length)], height);
      var i = Math.floor(Math.random()*xpos.length);
      this.start = createVector(xpos[i], height);
      this.end = createVector(xpos[i], 0);
    }

    return p;
  }

  this.start = this.pick();
  this.end = this.pick();
  this.current = this.start.copy();
  this.fgc = 0;
  this.bgc = 255;

  this.display = function() {
    strokeWeight(1);
    stroke(this.bgc, 200);
    if (this.lerpVal < 1) {
      this.current = p5.Vector.lerp(this.start, this.end, this.lerpVal);
      this.lerpVal += this.lerpInc;
      line(this.start.x, this.start.y, this.current.x, this.current.y);
    } else if (this.lerpVal < 2) {
      this.current = p5.Vector.lerp(this.start, this.end, this.lerpVal-1);
      this.lerpVal += this.lerpInc;
      line(this.end.x, this.end.y, this.current.x, this.current.y);
    } else {
      this.lerpVal = 0;
      // this.start = this.pick();
      // this.end = this.pick();
      this.pick();
    }
  }
}

function mouseReleased(){
  setup();
}
