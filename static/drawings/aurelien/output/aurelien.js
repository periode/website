var letters = [];

var x = 0;
var y = 0;

var timer = 5;
var max = 1;

var them = [];

var distance = 200;
var delta_y = 0;
var border;

var display_limit = 45;
var lines;
var start = false;

var inconsolata;

function preload(){
  lines = loadStrings("aurelien-parsed.txt");
}

function setup(){
  var cnv = createCanvas(windowWidth, windowHeight);
  them = [];
  border = width*0.2;
  x = border;
  y = height*0.9;
  background(0);
  populateStory();
  // lines = loadStrings("aurelien-parsed.txt", populateStory);
}

function populateStory(){
  letters = split(lines[0], '-');

  for(var i = 0; i < letters.length; i++){
    if((!letters[i]== "A" && letters[i] != "B" && letters[i] != ".")){
      y += 60;
      x = border;
    }

      them[i] = new Human(letters[i], x, y, i);
      x++;
      if(x > width-border){
        y += 20;
        x = border;
      }
  }

  for(var i = 0; i < them.length-1; i++){
    if(them[i].name == "A"){
      for(var j = i+2; j < them.length-1; j++){
        if((them[j].pos.x - them[i].pos.x) < distance && (them[j].pos.x - them[i].pos.x) > 0  && them[i].pos.y == them[j].pos.y && them[j].name == "B"){
          if(them[i].name != them[j].name)
            them[i].neighbors.push(them[j]);
        }
      }
    }

    if(them[i].name == "B"){
      for(var j = i+2; j < them.length-1; j++){
        if((them[j].pos.x - them[i].pos.x) < distance && (them[j].pos.x - them[i].pos.x) > 0  && them[i].pos.y == them[j].pos.y && them[j].name == "A"){
          if(them[i].name != them[j].name)
            them[i].neighbors.push(them[j]);
        }
      }
    }
  }

  start = true;
}

function draw(){
  if(start){
    background(0);
    push();
    translate(0, -delta_y);
    //scale(0.75);
    if(delta_y < them[them.length-1].pos.y)
      delta_y+=0.5;
    // noStroke();

    for(var i = 0; i < min(display_limit, them.length); i++){
      if(them[i].name == "A" || them[i].name == "B")
        them[i].display();
    }
    pop();

    noCursor();
    drawBorders();

    if(parseInt(delta_y) % 5 == 0 && display_limit < them.length)
      display_limit+=1025;
  }else{
    fill(255);
    text('yo', 100, 100);
  }
}

function drawBorders(){
  rectMode(CORNER);
  noStroke();
  fill(0, 20);
  for(var i = 0; i < 40; i++){
    rect(0, 0, width, 10+i);
  }

  for(var i = 0; i < 40; i++){
    rect(0, height, width, -10-i);
  }
}

var Human = function(letter, x, y, i) {
    this.name = letter;
    this.pos = createVector(x, y);
    this.ind = i;
    this.neighbors = [];
    this.alpha = 250;
    this.alpha_coeff = 0;
    this.col;

    if(this.name == "A"){
      this.size = 1;
      this.col = color(255, 150, 0);
    }else if(this.name == "B"){
      this.col = color(0, 150, 255);
      this.size = 1;
    }else{
      this.col = color(0, 0, 0);
      this.size = 0;
    }

    this.size+=random(4);

    this.theta = random(-PI/16, PI/16)+PI/4;
}

Human.prototype.display = function(){
    strokeWeight(1);
    fill(this.col, 100);
    strokeCap(PROJECT);
    for(var i = 0; i < this.neighbors.length; i++){
      stroke(red(this.col), green(this.col), blue(this.col), map((this.neighbors[i].pos.x - this.pos.x), 0, distance, this.alpha, this.alpha*0.25)*this.alpha_coeff);
      line(this.pos.x, this.pos.y, this.neighbors[i].pos.x, this.neighbors[i].pos.y);
    }

    if(this.alpha_coeff < 1)
      this.alpha_coeff+=0.01;
}
