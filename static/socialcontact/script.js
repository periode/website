var cnv;

var agentPosA;
var agentPosB;
var agentPosC;

//todo: add a cosine for the breathing

var agentSpeedA;
var agentSpeedB;
var agentSpeedC;

function setup(){
	var cnv = createCanvas(windowWidth, 200);
	//cnv.parent("canvas");
	cnv.position(0, 0);
	
	agentPosA = createVector(width*0.4, height*0.2);
	agentPosB = createVector(width*0.6, height*0.3);
	agentPosC = createVector(width*0.5, height*0.6);

	agentSpeedA = createVector(Math.random()-0.5, Math.random()-0.5);
	agentSpeedB = createVector(Math.random()-0.5, Math.random()-0.5);
	agentSpeedC = createVector(Math.random()-0.5, Math.random()-0.5);
}

function draw(){
	background(255, 255, 255);

	stroke(0);
	noFill();
	strokeWeight(1);
	for(var i = 0; i < 3; i++){
		ellipse(agentPosA.x, agentPosA.y, (i+2)*4, (i+2)*4);
	}
	strokeWeight(2);
	ellipse(agentPosA.x, agentPosA.y, 30, 30);

	stroke(0);
	strokeWeight(1);
	for(var i = 0; i < 4; i++){
		ellipse(agentPosB.x, agentPosB.y, (i+2)*4, (i+2)*4);
	}
	strokeWeight(2);
	ellipse(agentPosB.x, agentPosB.y, 30, 30);

	stroke(0);
	strokeWeight(1);
	for(var i = 0; i < 5; i++){
		ellipse(agentPosC.x, agentPosC.y, (i+2)*4, (i+2)*4);
	}
	strokeWeight(2);
	ellipse(agentPosC.x, agentPosC.y, 30, 30);


	//friendship AB
	strokeWeight(1);
	stroke(30, 110, 30, 50);
	for(var i = -2; i < 4; i++){
		line(agentPosA.x-((i+1)*4), agentPosA.y+(i*2), agentPosB.x+((i+1)*4), agentPosB.y-(i*2));
	}

	//love AC
	var avg = createVector((agentPosA.x+agentPosC.x)*0.5, (agentPosA.y+agentPosC.y)*0.5);

	noStroke();
	fill(220, 0, 0, 70);
	var r = dist(agentPosA.x, agentPosA.y, agentPosC.x, agentPosC.y);
	ellipse(avg.x, avg.y, r*0.5, r*0.5);
	ellipse(avg.x, avg.y, r, r);

	//power


	update();
}

function update(){
	agentPosA.x += agentSpeedA.x;
	agentPosA.y += agentSpeedA.y;

	agentPosB.x += agentSpeedB.x;
	agentPosB.y += agentSpeedB.y;

	agentPosC.x += agentSpeedA.x;
	agentPosC.y += agentSpeedA.y;

	reverseSpeed(agentPosA, agentSpeedA);
	reverseSpeed(agentPosB, agentSpeedB);
	reverseSpeed(agentPosC, agentSpeedA);
}

function reverseSpeed(pos, vec){
	if(pos.x < width*0.3){
		vec.x *= -1;
	}

	if(pos.x > width*0.7){
		vec.x *= -1;
	}

	if(pos.y < height*0.1){
		vec.y *= -1;
	}

	if(pos.y > height*0.9){
		vec.y *= -1;
	}	
}