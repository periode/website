var pos = [];
var fixed = [];
var target = [];
var letter = ["E", "X", "T", "I", "N", "C", "T", "I", "O", "N"];
var lerpVal = [];
var lerpInc = [];
var alpha = [];
var noiseVal = [];
var noiseInc = [];
var noiseCoeff = [];
var kerning;

var startTimeSwitch;
var timerSwitch;

var startTimeReset;
var timerReset;

var lerpLine;
var lerpLineInc;

var alphaAddition;
var alphaAdditionInc;

var hira;

var track;

function preload(){
	hira = loadFont("hira.otf");
  track = loadSound("audio/extinction3.mp3");
}

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight-50);
	cnv.position(0, 50);

	kerning = width*0.1;

  for (var i = 0; i < letter.length; i++) {
    pos[i] = createVector(width*0.5 - (letter.length*0.5)*kerning + (i+0.5)* kerning, height*0.5);
    fixed[i] = createVector(width*0.5 - (letter.length*0.5)*kerning + (i+0.5)* kerning, height*0.5);
    target[i] = createVector(width*0.5 - (letter.length*0.5)*kerning + (i+0.5)* kerning, height*0.5);
    lerpVal[i] = 0;
    lerpInc[i] = 0.001;
    noiseVal[i] = random();
    noiseInc[i] = 0.009;
    noiseCoeff[i] = 1.5;

    lerpLine = 2;
    lerpLineInc = 0.1;
  }

  startTimeSwitch = millis();
  timerSwitch = 10000;

  startTimeReset = millis();
  timerReset = 4000;

  alphaAddition = 0;
  alphaAdditionInc = 12;

	noCursor();

  track.play();
}

function draw(){
background(0);

  textSize(96);
  noStroke();
  textFont(hira);
  textAlign(CENTER, CENTER);
  for (var i = 0; i < letter.length; i++) {
    var n = turbulence(noiseVal[i], noiseCoeff[i]);

    fill(255, 15+2*alpha[i]+alphaAddition);

    text(letter[i], pos[i].x, pos[i].y);

    pos[i].x = lerp(pos[i].x, target[i].x, lerpVal[i]);
    if (lerpVal[i] < 1) lerpVal[i] += lerpInc[i];

    alpha[i] = 35*n;
    noiseVal[i] += noiseInc[i];
  }

  if (millis() - startTimeSwitch > timerSwitch) {
    switchLetters();
    if (timerSwitch > 3000) {
      timerSwitch -= 500;
    }
    startTimeSwitch = millis();
  }

  if (millis() - startTimeReset > timerReset) {
    resetLetters();
    if (timerReset < 15000) {
      timerReset += 500;
    }
    startTimeReset = millis();
  }

  if (lerpLine < 1) lerpLine += lerpLineInc;
  if (alphaAddition > 0) alphaAddition -= alphaAdditionInc;
}



function turbulence(val, coeff) {
  var r = 0;
  for (var i = 1; i < 8; i++) {
    r += noise(val*2*i)/(coeff*2*i);
  }
  return r;
}

function switchLetters() {
  for (var i = 0; i < lerpInc.length; i++) {
    lerpInc[i] = 0.001;
  }

  var a, b;
  a = (int)(Math.random()*letter.length);
  b = pick(a);

  var temp = target[a];

  target[a] = target[b];
  target[b] = temp;

  lerpVal[a] = 0;
  lerpVal[b] = 0;
}

function pick(f) {
  var i = (int)(Math.random()*letter.length);
  if (i == f) {
    return pick(f);
  } else {
    return i;
  }
}

function resetLetters() {
	console.log("reset");
  for (var i = 0; i < letter.length; i++) {
    target[i] = fixed[i];
    lerpVal[i] = 0;
    lerpLine = 0;
    alphaAddition = 255;
    lerpInc[i] = 0.1;
  }
}

function mousePressed() {
  var fs = fullScreen();
    if(mouseY > 100){
      fullScreen(!fs);
    }
}
