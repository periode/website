var noiseInc = 10;
var noiseVal = 0;
var step = 0;
var c = 255;

var centerStartTime = 0;
var centerTimer = 10000;

var verticalTimer = 240000;

var diagonalStartTime = 0;
var diagonalTimer = 600000;

var diagonalInnerStartTime = 0;
var diagonalInnerTimer = 900000;

var track;

function preload(){
	track = loadSound("audio/extinction.mp3");
}

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight-50);
	cnv.position(0, 50);

	noCursor();
	background(0);
  track.play();
  frameRate(25);
}

function draw(){
  stroke(c, 1);

  //horizontal
  line(0, height*0.5, step, (height*0.5)+(noise(noiseVal)-0.5)*500); //l
  line(width, height*0.5, width-step, (height*0.5)+(noise(noiseVal)-0.5)*500); //r

  if(millis() > verticalTimer){
    //vertical
    line(width*0.5, height*0.5, width*0.5+(noise(noiseVal)-0.5)*10000, (height*0.5)-step); //t
    line(width*0.5, height*0.5, width*0.5+(noise(noiseVal)-0.5)*10000, (height*0.5)+step); //b
  }

  if(millis() - diagonalStartTime > diagonalTimer){
    //top diagonal
    line(0, 0, width+(noise(noiseVal)-0.5)*1000, step); //top left to bottom right
    line(width, 0, 0-(noise(noiseVal)-0.5)*1000, step); //top right to bottom left

    //bottom diagonal
    line(0, height, width+(noise(noiseVal)-0.5)*1000, height-step); //bottom left to top right
    line(width, height, 0-(noise(noiseVal)-0.5)*1000, height-step); //bottom right to top left
  }

  if(millis() - diagonalInnerStartTime > diagonalInnerTimer){
    //top diagonal
    line(width*0.5, height*0.5, width*0.5+(noise(noiseVal)-0.5)*5000, height*0.5+step); //bottom right
    line(width*0.5, height*0.5, width*0.5-(noise(noiseVal)-0.5)*5000, height*0.5+step); //o bottom left

    //bottom diagonal
    line(width*0.5, height*0.5, width*0.5+(noise(noiseVal)-0.5)*5000, height*0.5-step); //bottom left to top right
    line(width*0.5, height*0.5, width*0.5-(noise(noiseVal)-0.5)*5000, height*0.5-step); //bottom right to top left
  }

  noiseVal += noiseInc;
  step += noiseInc;

  if(millis() - centerStartTime > centerTimer){
  	center();
  	centerStartTime = millis();
  }
}

function center(){
	console.log('centering');
	if(c == 0)
		c = 255;
 	else
  		c = 0;
}

function mousePressed() {
  var fs = fullScreen();
    if(mouseY > 100){
      fullScreen(!fs);
    }
}
