var letter= ["E", "X", "T", "I", "N", "C", "T", "I", "O", "N"];
var pos = [];
var kerning;

var hira;

var c0 = 0;
var c1 = 255;

var h = true;
var v = true;

var timerAlternate = 10000;
var startTimeAlternate = 0;

var timerInvert = 25000;
var startTimeInvert = 0;

var oscVal = 2;
var oscInc = 0.001;

var track;

function preload(){
	hira = loadFont("hira.otf");
  track = loadSound("audio/extinction1.mp3");
}

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight-50);
	cnv.position(0, 50);

	kerning = width*0.1;

  	for (var i = 0; i < letter.length; i++) {
    	pos[i] = createVector(width*0.5 - (letter.length*0.5)*kerning + (i+0.5)* kerning, height*0.5);
 	 }

   track.play();
}

function draw(){
	noCursor();

	background(c1);

  textAlign(CENTER);
  textSize(124);
  textFont(hira);

  var rand = Math.random();
  for (var i = 0; i < letter.length; i++) {
    if (rand < 0.75) {
      fill(c0);
      text(letter[i], pos[i].x, pos[i].y);
    } else {
      fill(c1);
      text(letter[i], pos[i].x, pos[i].y);
    }
  }

  for (var i = 0; i < width; i += 10) {
    var r = Math.random();
    if (r > map(cos(oscVal), -1, 1, 0.1, .9)) {
      stroke(c0, 255);
      strokeWeight(12+r*2);
      if(h)line(i, 0, i, height);
    }
  }

  for (var i = 0; i < width; i += 10) {
    var r = Math.random();
    if (r > map(sin(oscVal), -1, 1, 0.1, .9)) {
      stroke(c0);
      strokeWeight(12+r*2);
      if(v)line(0, i, width, i);
    }
  }

  oscVal += oscInc;

  if(millis() - startTimeAlternate > timerAlternate){
   alternate();
   startTimeAlternate = millis();
  }

    if(millis() - startTimeInvert > timerInvert){
   invert();
   startTimeInvert = millis();
  }
}

function invert(){
 if(c0 == 0){
  c0 = 255;
  c1 =0;
 }else{
  c0 = 0;
  c1 = 255;
 }
}

function alternate(){
 var r = random();
 if(r < 0.3){
   h = true;
   v = false;
 }else if(r < 0.6){
   v = true;
   h = false;
 }else if(r < 0.85){
   h = true;
   v = true;
 }else{
   v = false;
   h = false;
 }
}

function mousePressed() {
  var fs = fullScreen();
    if(mouseY > 100){
      fullScreen(!fs);
    }
}
