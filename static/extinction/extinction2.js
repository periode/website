var pos = [];
var w;
var h;
var col = [];
var stepW;
var stepH;
var cols;
var rows;
var modulo_black = 4;
var modulo_grey = 3;
var modulo_grey_light = 2;
var remainder = 0;
var remainder_inc = 1;

var timer_modulo = 500;
var starttime_modulo = 0;

var timer_black = 3400;
var starttime_black = 0;

var timer_grey = 5000;
var starttime_grey = 0;

var timer_grey_light = 5700;
var starttime_grey_light = 0;

var track;

function preload(){
	track = loadSound("audio/extinction2.mp3");
}

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight-50);
	cnv.position(0, 50);

	cols = 100;
	rows = 100;
	w = width/cols;
	h = height/rows;
	stepW = w;
	stepH = h;
	var i = 0;
	for(var x = 0; x < cols; x++){
		for(var y = 0; y < rows; y++){
			pos[i] = createVector(x*stepW, y*stepH);
			col[i] = 0;
			i++;
		}
	}

	noCursor();

	track.play();
}

function draw(){
	background(255);
	stroke(0);
	noStroke();
	for(var i = 0; i < pos.length; i++){
		if( i % modulo_black == remainder)
			fill(col[i]);
		else if(i % modulo_grey == remainder)
			fill(127 - col[i]);
		else if(i % modulo_grey_light == remainder)
			fill(190 - col[i]);
		else
			fill(255 - col[i]);

		rect(pos[i].x, pos[i].y, w, h);
	}

	if(millis() - starttime_black > timer_black){
		increase_black();
		starttime_black = millis();
	}

		if(millis() - starttime_grey > timer_grey){
		increase_grey();
		starttime_grey = millis();
	}

		if(millis() - starttime_grey_light > timer_grey_light){
		increase_grey_light();
		starttime_grey_light = millis();
	}

		if(millis() - starttime_modulo > timer_modulo){
		increase_modulo();
		starttime_modulo = millis();
	}
}

function increase_black(){
	modulo_black++;
}

function increase_grey(){
	modulo_grey++;
}

function increase_grey_light(){
	modulo_grey_light++;
}

function increase_modulo(){
	var r = Math.random();

	if(r > 0.75)
		remainder += remainder_inc;
	else
		remainder -= remainder_inc;

	if(remainder >= min(modulo_black, min(modulo_grey, modulo_grey_light)) || remainder == 0)
		remainder_inc *= -1;

		if(remainder < 0)
		remainder = 0;
}

function mousePressed() {
	var fs = fullScreen();
    if(mouseY > 100){
    	fullScreen(!fs);
    }
}
