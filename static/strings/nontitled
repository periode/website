it's surprisingly hard to stop thinking about you, and about the decision you made.

i liked you. i liked you and i still like you more than i'd like to admit. i liked you because i had never met someone like you. it turns out that you're an entire world that i would have loved to be part of. that you're a mystery i didn't have the chance to explore. that you are the person i expected the least to affect me that much. i could go on, but that's not the point. you're your own self with your feelings, choices and agency, but i am too, and it seemed that you dismissed that entirely. it was a cold, unnecessarily compact paragraph at the end of an unrelated message, and it hurt.

i'm not sure if you ever want to see me again. i'd like to say that it doesn't really matter, but i can't help but think, sometimes, about whether that might happen or not.

i still have that book of yours, and as long as i'll have it, there will be this possibility of a world where i might see you again. so i want to know if you want it back because, as long as it is on my shelf, you're also on my mind. if you don't think anything will ever happen again, then i'll mail it to you, or drop it off somewhere, or figure it out somehow, and things will go on as they do so well. if you don't want it back yet, then please, think about what that means to the person on the other end of that screen, and don't make that choice out of laziness, timidity, or lack of a better one.

maybe i'm totally out of line and maybe we're not on the same page and, if so, i apologize. it takes me an instant to think about these things and much longer to give them a voice but, again, maybe that means it would have been nicer to talk about that face to face, as intimidating as it might have seemed at the time.

anyway,

let me know,

i wish you the best of luck in your future endeavors.
