var maintext;

var pos = [];
var numPos = 800;
var dist_lines = 100;

var type;

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight*2);
	cnv.position(0, 0);
	type = Math.floor(Math.random()*4);
	setupGraphics();
}

function draw(){
	background(250, 250, 250);
	for(var i = 0; i < pos.length; i++){
		if(pos[i].x < width*0.25)
			stroke(0, 0, 255, 100);
		else if(pos[i].x < width*0.75)
			stroke(255, 255, 255, 100);
		else
			stroke(255, 0, 0, 100);

		noFill();
		if(type == 0)
			ellipse(pos[i].x, pos[i].y, 1, 1);
		else if(type == 1){
			ellipse(pos[i].x, pos[i].y, 3, 3);
		}else if(type == 2){
			rect(pos[i].x, pos[i].y, 3, 3);
		}else if(type == 3){
			line(pos[i].x-1, pos[i].y-1, pos[i].x+1, pos[i].y+1);
			line(pos[i].x-1, pos[i].y+1, pos[i].x+1, pos[i].y-1);
		}


		if(dist(mouseX, mouseY, pos[i].x, pos[i].y) < dist_lines)
			line(mouseX, mouseY, pos[i].x, pos[i].y);
	}
}

function touchStarted(){

}

function setupGraphics(){
	for(var i = 0; i < numPos; i++){
		if(i < numPos*0.5)
			var p = createVector(Math.random()*width*0.25, Math.random()*height);
		else
			var p = createVector(width*0.75 + Math.random()*width*0.25, Math.random()*height);
		pos.push(p);
	}
}

function pagePrincipale(){
console.log('click');
	open("index.html");
}