//-- thoughts

//- import './13_being.js'

class Idea extends Being{

  constructor(name, creator){
    this.realizers.append(creator)
    this.names.append(name)

    this.enrich = (creator) => {
      let horizon = ''
      for(let name in names){
        horizon += name+' and... '
      }
      console.log(creator,'has accessed',horizon)
    }
  }
}
