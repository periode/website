//-- for franz

class White extends Being {
  constructor(self){
    this.matter = true
    this.matters = true
  }

  static ness = () => {
    return this
  }
}

class NonWhite extends Being {
  constructor(other){
    this.matter = true
    this.matters = false
  }
}

//- every
new NonWhite(White.ness())