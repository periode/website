let world = 'tentatively at'

let limbo = function(peace, quiet, body){
  this.mind = world.indexOf(peace)
  this.quiet;

  this.rest = () => {
    if(this.mind < 0)
      requestAnimationFrame(body)
    else
      return;
  }
}

let now = new limbo("city", "hum", 8)

for (var aspiration in now) {
  if (aspiration.hasOwnProperty("rest")) {
    now.rest();
  }
}
