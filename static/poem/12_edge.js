//-- outside
sand = "....:..::.::::..::.::.:::::::..."
waves = "___-_--______---____--___------_"

//--inside
let pictured  = document.createElement('canvas')
let dream     = pictured.getContext('2d')

//-- stand here
you = window.navigator
thought = you.appName

//-- wish there
let vague = (swirl) => {
  if(!swirl)
    return

  err = ''
  amplitude = 32
  for(var flight = 0; flight < 1; flight+=0.03){
    if(Math.random() > flight)
      err += sand[Math.trunc(flight*amplitude)]
    else
      err += waves[Math.trunc(flight*amplitude)]
  }

  console.log(err);
}


step = 0.8

dream.beginPath()

for(var foot = 0; foot < waves.length; foot+=step){
  dream.moveTo(foot + sand.codePointAt(), foot + waves.codePointAt())
  vague(thought)
}

dream.closePath()
