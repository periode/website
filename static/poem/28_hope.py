class Hope:
	def _init_(_self_, trusted, dreamer):
		self.possible = trusted
		self.accountable = {'to': dreamer}
	def materialize(_self_):
		self.accountable = False

tomorrow = new Hope(True, "you")

tomorrow.materialize()
