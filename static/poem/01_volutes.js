//---volutes

let thoughts = ['', '', '', '', '', '', '', '', '']

let ashes
let desire

smoke()

function smoke(){
  exhale()
  ashes += sprinkle()
  live(ashes)
}

function live(ashes){
  if(thoughts.length > 1)
    thoughts.splice(0, 1);
  else
    return

  desire -= ashes

  setTimeout(smoke, 1000)
}

function sprinkle(){
  return 0.01
}

function exhale(){
  let cloud = ''
  for(var breath = 0; breath < Math.random()*10; breath++)
    cloud += '*'

  console.log(cloud);
}
