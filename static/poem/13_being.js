class Being {
    constructor(_n, _p, _s_t_t){
      this.name = _n == undefined ? 'in need of poetry' : _n
      this.presence = _p
      this.is_subject_to_time = _s_t_t

      this.might = () => { return Math.random() }
      this.could = 0.5
    }

    summon(){
      this.is_subject_to_time = true
      return this.is_subject_to_time
    }

    static echoes(sound){
      let utterance = ''

      let it = new Being("", true, false)
      for(let fragments of sound){
        for(let ricochets of sound){
          if(it.might() > it.could)
            utterance += fragments
        }
      }

      console.log(utterance)
    }
}
