var Ship = function(){
	this.pos = createVector(width*0.2, height*0.5);
	this.s = 20;
	this.theta = QUARTER_PI;
	this.thetaInc = 0.05;

	this.show = function(){
		push();
		rectMode(CENTER);
		translate(this.pos.x, this.pos.y);
		rotate(this.theta);
		stroke(255, 255, 255);
		fill(0, 0, 0);
		rect(0, 0, this.s, this.s);
		pop();
	}

	this.setPos = function(x, y){
		this.pos.x = x;
		this.pos.y = y;
	}

	this.incRot = function(){
		this.theta += this.thetaInc;
	}

	this.getMouseCollision = function(){
    var gap = 70;
		if(mouseX > this.pos.x - gap && mouseX < this.pos.x + gap && mouseY > this.pos.y - gap && mouseY < this.pos.x + gap){
			return true;
		}else{
			return false;
		}
	}
}
