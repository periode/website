var you;
var comet;
var planet;
var points = [];
var size = [];
var bright = [];
var stars = [];
var pointsNum = 80;
var distThresh = 100;
var starSpeed = 0.175;
var halo_alpha = 1;

var freqs = [523.251, 622.254, 739.989, 1046.50];

var main_reverb = new Tone.Freeverb(0.7, 3000).toMaster();
var star_filter = new Tone.Filter(400, "lowpass", -12);
var star_delay = new Tone.FeedbackDelay(1, 0.15);
star_delay.wet.value = 0.35;
star_delay.connect(main_reverb)
star_filter.connect(star_delay);


var drone = new Tone.FMSynth(500, 10);
var drone_filter = new Tone.Filter(400, "lowpass", -12);
var drone_lfo = new Tone.LFO(0.5, 10, 400);
var drone_reverb = new Tone.JCReverb(1).toMaster();
drone.volume.value = -40;
drone.envelope.attack = 10;
drone.envelope.sustain = 1;
drone.envelope.decay = 0;
drone.connect(drone_filter);

drone_lfo.connect(drone_filter.frequency);
drone_filter.connect(drone_reverb);

drone.triggerAttack(100);
drone_lfo.start();


var orator;

//TODO add sliders

function setup(){
	var cnv = createCanvas(windowWidth, windowHeight);
	cnv.position(0, 30);

	for(var i = 0; i < pointsNum; i++){
		size[i] = 1+Math.random()*3;
		bright[i] = 100+(Math.random()*155);
		points[i] = createVector(Math.random()*width*0.9+(width*0.3), Math.random()*height*0.85);
		stars[i] = new Star(points[i].x, points[i].y);
	}

	you = new Ship();
	comet = new Comet();
	planet = new Planet();
	noCursor();
}

function draw(){
	background(0, 0, 0);

	for(var i = 0; i < stars.length; i++){

		fill(255, 255, 255, bright[i]);
		noStroke();
		ellipse(stars[i].pos.x, stars[i].pos.y, size[i], size[i]);
		stars[i].display();

		if(dist(you.pos.x, you.pos.y, stars[i].pos.x, stars[i].pos.y) < distThresh){
			stars[i].update();
			//if(!stars[i].canPlay) this.stars[i].canPlay = true;
			line(you.pos.x, you.pos.y, stars[i].pos.x, stars[i].pos.y);
			if(stars[i].canPlay){
				stars[i].note.triggerAttackRelease(stars[i].frequency, 0.25);
				stars[i].canPlay = false;
			}
		}else{
			stars[i].setToZero();
		}

		//move stuff around
		if(i % 3 == 0)
			points[i].x -= starSpeed;
		else if(i % 3 == 1)
			points[i].x -= starSpeed*1.2;
		else
			points[i].x -= starSpeed*1.4;

		if(points[i].x < 0){
			points[i].x = width;
			var r = Math.random();
			if(r < 0.6){
				points[i].y = Math.random()*height*0.5;
			}else if(r < 0.9){
				points[i].y = Math.random()*height*0.25+height*0.5;
			}else{
				points[i].y = Math.random()*height*0.25+height*0.75;
			}
		}

		stars[i].setPos(points[i].x, points[i].y);
	}

	if(mouseIsPressed && you.getMouseCollision()){
		you.setPos(mouseX, mouseY);
		you.incRot();
	}

	comet.display();
	comet.update();
	comet.setFilter();


	planet.display();
	planet.update();
	planet.setFilter();
	you.show();

	if(comet.getPosX() < -100){
		comet.setPos(Math.random()*width+width, Math.random()*height*0.2);
	}

	if(planet.getPosX() < -100){
		planet.setPos(Math.random()*width+width, Math.random()*height*0.6+height*0.2);
	}

	//cursor
	stroke(255, 255, 255);
	fill(255, 255, 255);
	line(mouseX-4, mouseY, mouseX+4, mouseY);
	line(mouseX, mouseY-4, mouseX, mouseY+4);

	drawHalo();
}


function drawHalo(){
		noStroke();

		for(var i = 0; i < 20; i++){
			fill(255, halo_alpha);
			arc(width*0.5, height*4.575+i*2, width*4, width*4, 2*PI*8.5/12, 2*PI*9.5/12);
		}

}
