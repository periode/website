var Comet = function(){
	// this.pos = createVector(Math.random()*width*2+width, height*0.2);
  this.pos = createVector(Math.random()*width, height*0.2);
	this.speed = 1;
	this.size = 40;
	this.stepSize = 50;
	this.alpha = 255;
  this.alphaInc = 3;

  this.thresh = width*0.75;

	this.trailNum = 2;
	this.trailDist = 20;
	this.trailVal = 0;
	this.trailInc = 0.1;
	this.randInc = [Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10];

  this.filter = new Tone.Filter(500, "lowpass", -48).connect(main_reverb);

  this.chorus = new Tone.Chorus(0.5, 2.5, 0.5).connect(this.filter);
  this.note = new Tone.FMSynth().connect(this.chorus);
  this.note.modulationIndex.value = 10;
  this.note.modulation.type.value = "saw";
  this.frequency = 290;

  // this.note.set("noise.type", "brown");
  this.note.volume.value = -15;
  this.note.envelope.attack = 1;
  this.note.envelope.sustain = 0.25;
  this.note.envelope.decay = 0.25;
  this.note.envelope.release = 3;




	this.display = function(){
		push();
		translate(this.pos.x, this.pos.y);
		fill(255, 255, 255);
		strokeWeight(2);
		stroke(255, 255, 255, 150);
		beginShape();
		var i = 0;
		for(var j = 0; j < 360; j += this.stepSize){
			vertex(sin(radians(j))*this.size*0.5+this.randInc[i], (cos(radians(j))*this.size*0.5+this.randInc[i]));
			i++;
		}
		endShape(CLOSE);
		pop();
	}

	this.update = function(){
		this.pos.x -= this.speed;
		if(this.pos.x < width && this.pos.x > 0){
			//this.note.start();
		}else{
			//this.note.stop();
		}

    // this.note.volume.value = map(constrain(p5.Vector.dist(this.pos, you.pos), 0, width), 0, width, -40, 100);

    this.alpha -= this.alphaInc;

    if(this.alpha < -100)
      this.setToZero();

    if(dist(you.pos.x, you.pos.y, this.pos.x, this.pos.y) < this.thresh){
  		stroke(255, this.alpha)
  		line(you.pos.x, you.pos.y, this.pos.x, this.pos.y);
  		if(this.canPlay){
  			this.note.triggerAttackRelease(this.frequency, 1.5);
  			this.canPlay = false;
  		}
  	}else{
      // this.note.triggerRelease();
  	}
	}

	this.getPosX = function(){
		return this.pos.x;
	}

	this.setPos = function(y){
		this.randInc = [Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10];
		this.pos.x = Math.random()*width*4+width;
		this.pos.y = y;
	}

  this.setToZero = function(){
    this.alpha = 255;
    this.frequency = random(200, 300);
    this.canPlay = true;
  }

	this.setFilter = function(){

	}
}
