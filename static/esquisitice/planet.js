var Planet = function(){
	this.pos = createVector(Math.random()*width*2+width, height*0.4);
	this.speed = 0.25;
	this.rad = 200;
	this.radInc = 0.65+Math.random()+0.45;
	this.theta = Math.random()*360;
	this.thetaInc = 0.001;
	this.stepSize = 50;
	this.linesNum = parseInt(Math.random()*3);
	this.alpha = 255;

	this.trailNum = 2;
	this.trailDist = 20;
	this.trailVal = 0;
	this.trailInc = 0.1;
	this.randInc = [Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10, Math.random()*10];

	this.display = function(){
		push();
		translate(this.pos.x, this.pos.y);
		rotate(this.theta);
		stroke(255, 255, 255, 170);
		fill(0, 0, 0);
		ellipse(0, 0, this.rad, this.rad);
		for(var i = -1; i < this.linesNum; i++){
			line(-this.rad*this.radInc*0.65-(i*2), -i*4, this.rad*this.radInc*0.65-(i*2), -i*4);
		}

		pop();
	}

	this.update = function(){
		this.pos.x -= this.speed;
		this.theta += this.thetaInc;
		if(this.pos.x < width && this.pos.x > 0){
			//this.noise.start();
		}else{

		}
	}

	this.getPosX = function(){
		return this.pos.x;
	}

	this.setPos = function(x, y){
		this.theta = Math.random()*TWO_PI;
		this.linesNum = parseInt(Math.random()*3);
		this.pos.x = x;
		this.pos.y = y;
	}

	this.setFilter = function(){
		var n = noise(this.filterFreq);
		this.alpha = n*50+50;
		this.filterFreq += this.filterInc;
		if(n < 0.3){
			// this.noise.amp(0);
		}else{
			// this.noise.amp(Math.random()*0.5+0.5);
		}
	}
}
