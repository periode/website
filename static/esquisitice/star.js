var Star = function(x, y){
	this.pos = createVector(x, y);
	this.col = color(255, 255, 255);

	this.rad = 0;
	this.radInc = 4;

	this.stepSize = 30+(Math.random()*60);

	this.alpha = 255;
	this.alphaInc = 5;

	//sound
  var options = {
    oscillator:{type:"triangle"},
    envelope:{
      attack: 0.01,
      decay:0.0,
      sustain:0.5,
      release:1
    }
  };
	this.note = new Tone.Synth(options);
  this.note.connect(star_filter);

  this.note.volume.value = -30;
  this.frequency = map(this.pos.y, 0, height, 1500, 500);
	this.canPlay = true;
  this.firstTime = true;

	this.display = function(){
		noFill();
		stroke(this.col, this.alpha);
		push();
		beginShape();
		for(var i = 0; i < 360; i += this.stepSize){
			vertex(this.pos.x+this.rad*sin(radians(i)), this.pos.y+this.rad*cos(radians(i)));
		}
		endShape(CLOSE);
		pop();
	}

	this.update = function(){
		this.col = color(255, 255, 255, this.alpha);
		this.rad += this.radInc;
		this.alpha -= this.alphaInc;

		if(this.alpha < 0)
			//this.note.stop();

		if(this.alpha < -200)
			this.setToZero();
	}

	this.setToZero = function(){
		this.rad = 0;
		this.alpha = 255;
		this.canPlay = true;
	}

	this.setPos = function(x, y){
		this.pos.x = x;
		this.pos.y = y;
	}

	this.getPosX = function(){
		return this.pos.x;
	}
}
