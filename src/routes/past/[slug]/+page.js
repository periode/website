import { projects } from '../projects.js';
import { error } from '@sveltejs/kit';

export function load({ params }) {
	const project = projects.find((proj) => proj.slug === params.slug)
	if(!project)  throw error (404)
	return { project };
}
