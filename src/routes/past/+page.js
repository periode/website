import { projects } from './projects.js';

export function load() {
	const ongoing = projects.filter((p) => {return p.status == "ongoing"})
	const archived = projects.filter((p) => {return p.status == "archived"})
	return { ongoing: ongoing, archived: archived };
}
