import { readdirSync } from 'fs';
import { error } from '@sveltejs/kit';

export function load({ params }) {
    const stills = readdirSync(`static/drawings/stills/${params.slug}/`)
    if (!stills || stills.length == 0) throw error(404)
    return { slug: params.slug, stills: stills };
}
