.DEFAULT_GOAL:=help
SHELL:=/usr/bin/bash

.PHONY: public build

sync: ## upload the remote folder
	rsync -Pzauvr --delete build/* enframed:/var/www/pierre

build: ## buils the static files
	echo "PUBLIC_BUILD_DATE=$(shell date -u +%d.%m.%Y)" > .env
	yarn build

date:
	echo "PUBLIC_BUILD_DATE=$(shell date -u +%d.%m.%Y)" > .env

deploy: build sync ## build and sync


help: ## display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
